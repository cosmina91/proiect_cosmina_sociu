## Wantsome - Jewellery Online Store

### 1. Description

This is an application which allows the user to see the products, filter them and place orders.
Products are grouped into different categories.

Supported actions:

- add/delete products from cart and update product`s quantity
- filter the products by category
- filter the products with the 'search' button
- validate and insert into database the user's personal informations like the shipping address, name and e-mail
- updating the stock for each product
- add into database the order with the current date, client id and product id
- computes the total price for each item, multiplied by the quantity that the user has selected
- computes the total price for all the products on the cart

---

---

### 2. Setup

No setup needed, just start the application. If the database is missing, it will create a new database (of type SQLite,
stored in a local file named 'Online_Store_Final_Project_DB.db'), and use it to save the future data.

Once started, access it with a web browser at: <http://localhost:8080>

---

### 3. Technical details

__Technologies__

- main code is written in Java (version 11)
- it uses [Javalin](https://javalin.io/) micro web framework (which includes an embedded web server, Jetty)
- it uses [Velocity](https://velocity.apache.org/) templating engine, to separate the UI code from Java code; UI code
  consists of basic HTML and CSS code
- it uses [SQLite](https://www.sqlite.org/), a small embedded database, for its persistence, using SQL and JDBC to
  access it from Java code

  The tables structure:

 ![](D:\Curs_java\proiect_cosmina_sociu\src\main\resources\public\SQLiteTables.png)

__Code structure__

- java code is organized in packages by its role, on layers:
  - db - database part, including DTOs and DAOs, as well as the code to init
    and connect to the db
  - ui - code related to the interface/presentation layer

- web resources are found in `main/resources` folder:
  - under `/public` folder - static resources to be served by the web server
    directly (images, css files)
  - all other (directly under `/resources`) - the Velocity templates
  
---

### 4. Future plans

- making a multi user login page
- adding some new filters (eg. filter by price)
- adding an order history page for each user