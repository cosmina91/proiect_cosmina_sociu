package wantsome.project;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import wantsome.project.db.address.AddressBL;
import wantsome.project.db.address.address_dao.AddressDAO;
import wantsome.project.db.address.address_dao.impl.SQLiteAddressDAO;
import wantsome.project.db.clients.ClientsBL;
import wantsome.project.db.clients.clients_dao.ClientsDAO;
import wantsome.project.db.clients.clients_dao.impl.SQLiteClientsDAO;
import wantsome.project.db.order_item.OrderItemBL;
import wantsome.project.db.order_item.order_item_dao.OrderItemDAO;
import wantsome.project.db.order_item.order_item_dao.impl.SQLiteOrderItemDAO;
import wantsome.project.db.products.ProductsBL;
import wantsome.project.db.products.products_dao.ProductsDAO;
import wantsome.project.db.products.products_dao.impl.SQLiteProductsDAO;
import wantsome.project.db.service.DbInitService;
import wantsome.project.ui.ClientsController;
import wantsome.project.ui.ProductsController;

import java.io.IOException;


public class OnlineStoreWebApp {


    public static void main(String[] args) throws IOException {

        setup();
        startWebServer();

    }

    private static void setup() throws IOException {
        DbInitService.createMissingTables();
        DbInitService.insertSampleData();
    }

    private static void startWebServer() {
        ProductsDAO productsDAO = new SQLiteProductsDAO();
        ProductsBL productsBL = new ProductsBL(productsDAO);
        OrderItemDAO orderItemDAO = new SQLiteOrderItemDAO();
        OrderItemBL orderItemBL = new OrderItemBL(orderItemDAO);
        ClientsDAO clientsDAO = new SQLiteClientsDAO();
        ClientsBL clientsBL = new ClientsBL(clientsDAO);
        AddressDAO addressDAO = new SQLiteAddressDAO();
        AddressBL addressBL = new AddressBL(addressDAO);

        ProductsController productsController = new ProductsController(productsBL, orderItemBL);
        ClientsController clientsController = new ClientsController(clientsBL, addressBL);

        Javalin app = Javalin.create(config -> {
            config.addStaticFiles("/public", Location.CLASSPATH); //location of static resources (like images, .css ..), relative to /resources dir
            config.enableDevLogging(); //extra logging, for development/debug
        }).start();

        app.get("/", ctx -> ctx.redirect("/main"));
        app.get("/main", productsController::getTotalQuantity);
        app.get("/all_products", productsController::getAllProducts);
        app.get("/rings", productsController::getAllRings);
        app.get("/bracelets", productsController::getAllBracelets);
        app.get("/earrings", productsController::getAllEarrings);
        app.get("/necklaces", productsController::getAllNecklaces);
        app.get("/pendants", productsController::getAllPendants);
        app.get("/cart", productsController::getAllCartItems);
        app.post("/addToCart", productsController::addItemToCartList);
        app.post("/remove", productsController::removeItemFromCartList);
        app.get("/orderForm", clientsController::getOrderForm);
        app.post("/orderForm", clientsController::validateClientInfo);
        app.get("/pay", clientsController::getPaymentPage);
        app.post("/pay", clientsController::verifyCardInfo);
        app.get("/invoice", clientsController::getInvoicePage);
        app.post("/invoice", clientsController::backToMainPage);
        app.post("/search", ctx -> productsController.getSearchedProducts(productsBL, ctx));

    }


}
