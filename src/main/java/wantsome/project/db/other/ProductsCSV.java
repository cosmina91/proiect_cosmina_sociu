package wantsome.project.db.other;

import wantsome.project.db.products.ProductsDTO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class ProductsCSV {

    private final ProductsDTO productsDTO;

    public ProductsCSV(ProductsDTO productsDTO) {
        this.productsDTO = productsDTO;

    }

    //the method to read from a CSV file
    public static List<ProductsDTO> readFromCSVFile(String path) throws IOException {
        return readCSV(new FileReader(path));
    }

    public String toCSV() {
        return productsDTO.getProductType() + "," + productsDTO.getDescription() + "," +
                productsDTO.getPrice() + "," + productsDTO.getStock() + "," + productsDTO.getProductPath();
    }

    public static ProductsDTO fromCSV(String line) {
        String[] productProperties = new String[]{};
        ProductType productTypeCSV = null;
        double priceCSV = 0;
        int stockCSV = 0;
        String descriptionCSV = "";
        String productPath = "";
        try {
            productProperties = line.split(",");
            if (validateArrayLength(productProperties)) {
                productTypeCSV = ProductType.getValue(productProperties[0]);
                descriptionCSV = productProperties[1];
                priceCSV = Double.parseDouble(productProperties[2]);
                stockCSV = Integer.parseInt(productProperties[3]);
                productPath = productProperties[4];
            }
        } catch (Exception ex) {
            System.out.println("Error reading line from CSV! " + ex.getMessage());
        }
        return new ProductsDTO(productTypeCSV, descriptionCSV, priceCSV, stockCSV, productPath);
    }

    private static boolean validateArrayLength(String[] productProperties) {
        if (productProperties.length != 5) {
            System.err.println("String[] length after splitting the line from CSV must be 5! Actual length:" + productProperties.length);
            return false;
        }
        return true;
    }

    private static List<ProductsDTO> readCSV(Reader reader) {
        List<ProductsDTO> deserializedProductsDTO = new ArrayList<>();

        try (BufferedReader readCSV = new BufferedReader(reader)) {

            String line;

            while ((line = readCSV.readLine()) != null) {
                deserializedProductsDTO.add(fromCSV(line));
            }
        } catch (IOException ex) {
            System.out.println("Error reading line from csv! " + ex.getMessage());
        }
        return deserializedProductsDTO;
    }


}
