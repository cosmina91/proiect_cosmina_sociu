package wantsome.project.db.other;

public enum ProductType {
    INELE("Inele"),
    BRATARI("Bratari"),
    CERCEI("Cercei"),
    COLIERE("Coliere"),
    PANDANTIVE("Pandantive");

    private final String label;

    ProductType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static ProductType getValue(String value) {
        for (ProductType p : values()) {
            if (p.label.equalsIgnoreCase(value)) {
                return p;
            }
        }
        return null;
    }
}
