package wantsome.project.db.orders;

import java.sql.Date;
import java.util.Objects;

public class OrdersDTO {
    private long id;
    private long clientId;
    private Date fulfillDate;
    private double totalPrice;

    public OrdersDTO(long id, long clientId, Date fulfillDate, double totalPrice) {
        this.id = id;
        this.clientId = clientId;
        this.fulfillDate = fulfillDate;
        this.totalPrice = totalPrice;
    }

    public OrdersDTO(long clientId, Date fulfillDate, double totalPrice) {
        this.clientId = clientId;
        this.fulfillDate = fulfillDate;
        this.totalPrice = totalPrice;
    }

    public long getId() {
        return id;
    }

    public long getClientId() {
        return clientId;
    }

    public Date getFulfillDate() {
        return fulfillDate;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    @Override
    public String toString() {
        return "OrdersDTO{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", fulfillDate=" + fulfillDate +
                ", totalPrice=" + totalPrice +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdersDTO ordersDTO = (OrdersDTO) o;
        return id == ordersDTO.id && clientId == ordersDTO.clientId && Double.compare(ordersDTO.totalPrice, totalPrice) == 0 && Objects.equals(fulfillDate, ordersDTO.fulfillDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, fulfillDate, totalPrice);
    }
}
