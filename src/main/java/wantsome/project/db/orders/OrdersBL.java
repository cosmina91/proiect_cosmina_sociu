package wantsome.project.db.orders;

import wantsome.project.db.orders.orders_dao.OrdersDAO;

import java.sql.Date;
import java.util.List;

public class OrdersBL {
    private final OrdersDAO ordersDAO;

    public OrdersBL(OrdersDAO ordersDAO) {
        this.ordersDAO = ordersDAO;
    }

    public void addOrder(long clientId, Date fulfillDate, double totalPrice) {
        if (!validateClientId(clientId) || !validateTotalPrice(totalPrice)) {
            return;
        }
        OrdersDTO ordersDTO = new OrdersDTO(clientId, fulfillDate, totalPrice);
        ordersDAO.insert(ordersDTO);
    }

    public long getOrderId(long clientId, Date fulfillDate, double price) {
        return ordersDAO.selectOrderId(clientId, fulfillDate, price);
    }

    public List<OrdersDTO> getCurrentOrdersForClient(long clientId) {
        if (!validateClientId(clientId)) {
            return null;
        }
        return ordersDAO.selectCurrentOrdersForClient(clientId);
    }

    public List<OrdersDTO> getClosedOrdersForClient(long clientId) {
        if (!validateClientId(clientId)) {
            return null;
        }
        return ordersDAO.selectClosedOrdersForClient(clientId);
    }

    public OrdersDTO getOrderDetailsById(long id) {
        return ordersDAO.selectOrdersDetailsById(id);
    }

    private boolean validateClientId(long clientId) {
        if (clientId > 0) {
            return true;
        }
        System.err.println("Invalid client id! Must be a positive number! ");
        return false;
    }

    private boolean validateTotalPrice(double totalPrice) {
        if (totalPrice >= 0) {
            return true;
        }
        System.err.println("Invalid total price! Must be a positive number! ");
        return false;
    }
}
