package wantsome.project.db.orders.orders_dao;

import wantsome.project.db.orders.OrdersDTO;

import java.sql.Date;
import java.util.List;

public interface OrdersDAO {
    void insert(OrdersDTO ordersDTO);

    List<OrdersDTO> selectCurrentOrdersForClient(long clientId);

    List<OrdersDTO> selectClosedOrdersForClient(long clientId);

    OrdersDTO selectOrdersDetailsById(long id);

    long selectOrderId(long clientId, Date fulfillDate, double price);

}
