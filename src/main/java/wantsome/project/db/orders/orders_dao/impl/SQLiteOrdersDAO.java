package wantsome.project.db.orders.orders_dao.impl;

import wantsome.project.db.orders.OrdersDTO;
import wantsome.project.db.orders.orders_dao.OrdersDAO;
import wantsome.project.db.service.DbManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLiteOrdersDAO implements OrdersDAO {
    private static final String INSERT_ORDER = "insert into ORDERS (client_id, fulfill_date, total_price)" +
            "values(?, ?, ?);";
    private static final String SELECT_CURRENT_ORDER_FOR_CLIENT = "select * from ORDERS where client_id = ? and fulfill_date is null and total_price is null;";
    private static final String SELECT_CLOSED_ORDER_FOR_CLIENT = "select * from ORDERS where client_id = ? and fulfill_date is not null and total_price is not null;";
    private static final String SELECT_ORDER_DETAILS_BY_ID = "select * from ORDERS where id = ?;";
    private static final String SELECT_ORDER_ID = "select id from ORDERS where client_id = ? and fulfill_date = ? and total_price = ?;";


    @Override
    public void insert(OrdersDTO ordersDTO) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_ORDER);) {
            int index = 0;
            statement.setLong(++index, ordersDTO.getClientId());
            statement.setDate(++index, ordersDTO.getFulfillDate());
            statement.setDouble(++index, ordersDTO.getTotalPrice());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting order! " + e.getMessage());
        }
    }

    @Override
    public List<OrdersDTO> selectCurrentOrdersForClient(long clientId) {
        List<OrdersDTO> currentOrders = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_CURRENT_ORDER_FOR_CLIENT);) {
            statement.setLong(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            addingOrdersFromResultSet(resultSet, currentOrders);
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting current orders for client! " + e.getMessage());
        }
        return currentOrders;
    }

    @Override
    public List<OrdersDTO> selectClosedOrdersForClient(long clientId) {
        List<OrdersDTO> closedOrders = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_CLOSED_ORDER_FOR_CLIENT);) {
            statement.setLong(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            addingOrdersFromResultSet(resultSet, closedOrders);
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting closed orders for client! " + e.getMessage());
        }
        return closedOrders;
    }

    @Override
    public OrdersDTO selectOrdersDetailsById(long id) {
        OrdersDTO orderById = null;
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ORDER_DETAILS_BY_ID);) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                orderById = new OrdersDTO(resultSet.getLong("id"), resultSet.getLong("client_id"), resultSet.getDate("fulfill_date"), resultSet.getDouble("total_price"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting orders by id! " + e.getMessage());
        }
        return orderById;
    }

    @Override
    public long selectOrderId(long clientId, Date fulfillDate, double price) {
        List<Long> orderIds = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ORDER_ID);) {
            statement.setLong(1, clientId);
            statement.setDate(2, fulfillDate);
            statement.setDouble(3, price);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderIds.add(resultSet.getLong("id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting order id! " + e.getMessage());
        }
        return orderIds.stream().distinct().findFirst().orElse((long) -1);
    }

    private void addingOrdersFromResultSet(ResultSet resultSet, List<OrdersDTO> currentOrders) throws SQLException {
        while (resultSet.next()) {
            currentOrders.add(new OrdersDTO(resultSet.getLong("id"), resultSet.getLong("client_id"), resultSet.getDate("fulfill_date"), resultSet.getDouble("total_price")));
        }
    }
}
