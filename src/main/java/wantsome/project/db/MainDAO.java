package wantsome.project.db;

import wantsome.project.db.products.ProductsBL;
import wantsome.project.db.products.products_dao.ProductsDAO;
import wantsome.project.db.products.products_dao.impl.SQLiteProductsDAO;
import wantsome.project.db.service.DbManager;

import java.io.IOException;
import java.sql.SQLException;

public class MainDAO {
    public static void main(String[] args) throws IOException, SQLException {
        DbManager.getConnection();

        //Testing Address
//        AddressDAO addressDAO = new SQLiteAddressDAO();
//        AddressBL addressBL = new AddressBL(addressDAO);
//        addressBL.addAddress("Ala", "2bala", "portocala", "1234c");
//        addressBL.deleteAddress(2);

        //Testing Clients
//        ClientsDAO clientsDAO = new SQLiteClientsDAO();
//        ClientsBL clientsBL = new ClientsBL(clientsDAO);
//        clientsBL.addClient("Sociu", "Cosmina", "sociucosmina@yahoo.com", "Cosmian12!", 3 );

        //Testing Orders
//        OrdersDAO ordersDAO = new SQLiteOrdersDAO();
//        OrdersBL ordersBL = new OrdersBL(ordersDAO);
//        ordersBL.addOrder(2, Date.valueOf("2022-05-01"), 12.30);
//        System.out.println(ordersBL.getCurrentOrdersForClient(2));
//        System.out.println(ordersBL.getClosedOrdersForClient(2));

        //Testing Order Item
//        OrderItemDAO orderItemDAO = new SQLiteOrderItemDAO();
//        OrderItemBL orderItemBL = new OrderItemBL(orderItemDAO);
//        orderItemBL.addOrderItem(1,1,1);
//        orderItemBL.deleteOrderItem(3);
//        orderItemBL.updateQuantity(1, 5);

        //Testing Products
        ProductsDAO productsDAO = new SQLiteProductsDAO();
        ProductsBL productsBL = new ProductsBL(productsDAO);
//        List<ProductsDTO> productList = ProductsCSV.readFromCSVFile("D:\\Curs_java\\proiect_cosmina_sociu\\src\\main\\java\\wantsome\\project\\db\\other\\products.csv");
//        productList.stream().forEach(p -> productsBL.addProduct(p.getProductType(), p.getDescription(), p.getPrice(), p.getStock()));

//        List<ProductsDTO> productList = productsBL.getAllProducts();
//        productList.stream().forEach(p -> System.out.println(p));

//        System.out.println(productsBL.getProductById(2));

//        List<ProductsDTO> productsByType = productsBL.getAllProductsByType(ProductType.getValue("Coliere"));
//        productsByType.stream().forEach(p -> System.out.println(p));

//        productsBL.updateStock(1, 40);


    }
}
