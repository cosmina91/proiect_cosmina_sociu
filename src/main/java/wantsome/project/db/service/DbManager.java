package wantsome.project.db.service;

import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbManager {
    //    private static String JDBC_URL = "C:\\Users\\Admin\\Desktop\\Proiect final Online Store\\Online_Store_Final_Project_DB";
    static final String JDBC_URL = "jdbc:sqlite:Online_Store_Final_Project_DB.db";
//    static final String JDBC_URL = "jdbc:sqlite:C:\\Users\\Admin\\Desktop\\Proiect final Online Store\\Online_Store_Final_Project_DB.db";

    public static Connection getConnection() throws SQLException {
        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true);
        config.setDateClass("text");
        config.setDateStringFormat("yyyy-MM-dd");
        return DriverManager.getConnection(JDBC_URL, config.toProperties());
    }
}
