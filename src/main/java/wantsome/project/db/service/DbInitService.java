package wantsome.project.db.service;

import wantsome.project.db.other.ProductsCSV;
import wantsome.project.db.products.ProductsBL;
import wantsome.project.db.products.ProductsDTO;
import wantsome.project.db.products.products_dao.ProductsDAO;
import wantsome.project.db.products.products_dao.impl.SQLiteProductsDAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DbInitService {
    private static final String CREATE_ADDRESS_SQL = "create table if not exists ADDRESS(" +
            "id integer primary key autoincrement," +
            "street varchar(200) not null," +
            "street_number varchar(20) not null," +
            "town varchar(50) not null," +
            "postal_code varchar(20) not null" +
            ");";
    private static final String CREATE_CLIENTS_SQL = "create table if not exists CLIENTS(" +
            "id integer primary key autoincrement," +
            "first_name varchar(50) not null," +
            "last_name varchar(50) not null," +
            "email varchar(100) unique not null," +
            "password varchar(15) not null," +
            "address_id integer references ADDRESS(id) not null" +
            ");";
    private static final String CREATE_PRODUCTS_SQL = "create table if not exists PRODUCTS(\n" +
            "id integer primary key autoincrement,\n" +
            "product_type varchar(50),\n" +
            "description varchar(100) unique not null,\n" +
            "price real not null,\n" +
            "stock integer not null,\n" +
            "product_path varchar(200) unique not null\n" +
            ");";
    private static final String CREATE_ORDERS_SQL = "create table if not exists ORDERS(" +
            "id integer primary key autoincrement," +
            "client_id integer references CLIENTS(id) not null," +
            "fulfill_date date," +
            "total_price real" +
            ");";
    private static final String CREATE_ORDER_ITEM_SQL = "create table if not exists ORDER_ITEM(" +
            "id integer primary key autoincrement," +
            "order_id integer references ORDERS(id)," +
            "product_id integer references PRODUCTS(id)," +
            "quantity integer" +
            ");";

    public static void createMissingTables() {
        try (Connection connection = DbManager.getConnection();
             Statement statement = connection.createStatement();) {

            statement.execute(CREATE_ADDRESS_SQL);
            statement.execute(CREATE_CLIENTS_SQL);
            statement.execute(CREATE_PRODUCTS_SQL);
            statement.execute(CREATE_ORDERS_SQL);
            statement.execute(CREATE_ORDER_ITEM_SQL);
        } catch (SQLException ex) {
            throw new RuntimeException("Error creating missing tables! " + ex.getMessage());
        }
    }

    public static void insertSampleData() throws IOException {
        ProductsDAO productsDAO = new SQLiteProductsDAO();
        if (productsDAO.selectAll().isEmpty()) {
            ProductsBL productsBL = new ProductsBL(productsDAO);
            List<ProductsDTO> productList = ProductsCSV.readFromCSVFile("src\\main\\java\\wantsome\\project\\db\\other\\products.csv");
            productList.stream().forEach(p -> productsBL.addProduct(p.getProductType(), p.getDescription(), p.getPrice(), p.getStock(), p.getProductPath()));
        }
    }


}
