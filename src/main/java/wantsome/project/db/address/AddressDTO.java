package wantsome.project.db.address;

import java.util.Objects;

public class AddressDTO {
    private long id;
    private String street;
    private String streetNumber;
    private String town;
    private String postalCode;

    public AddressDTO(long id, String street, String streetNumber, String town, String postalCode) {
        this.id = id;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
        this.postalCode = postalCode;
    }

    public AddressDTO(String street, String streetNumber, String town, String postalCode) {
        id = -1;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
        this.postalCode = postalCode;
    }

    public long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getTown() {
        return town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "AddressDTO{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", town='" + town + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressDTO that = (AddressDTO) o;
        return id == that.id && Objects.equals(street, that.street) && Objects.equals(streetNumber, that.streetNumber) && Objects.equals(town, that.town) && Objects.equals(postalCode, that.postalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, street, streetNumber, town, postalCode);
    }
}
