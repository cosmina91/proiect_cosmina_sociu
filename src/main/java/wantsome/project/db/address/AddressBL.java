package wantsome.project.db.address;

import wantsome.project.db.address.address_dao.AddressDAO;

public class AddressBL {
    private final AddressDAO addressDAO;

    public AddressBL(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    public void addAddress(String street, String streetNumber, String town, String postalCode) {
        if (!validateStreet(street) || !validateStreetNumber(streetNumber)
                || !validateTown(town) || !validatePostalCode(postalCode)) {
            return;
        }
        AddressDTO addressDTO = new AddressDTO(street, streetNumber, town, postalCode);
        addressDAO.insert(addressDTO);
    }

    public void deleteAddress(long id) {
        addressDAO.delete(id);
    }

    public void getAddressId(String street, String streetNumber, String town, String postalCode) {
        addressDAO.selectAddressId(street, streetNumber, town, postalCode);
    }

    public boolean validateStreet(String street) {
        if (street != null && !street.trim().isEmpty() && street.length() > 0 && street.length() <= 200) {
            return true;
        }
        System.err.println("Invalid street! Street length must be between 1 and 200!");
        return false;
    }

    public boolean validateStreetNumber(String streetNumber) {
        if (streetNumber != null && !streetNumber.trim().isEmpty() && streetNumber.length() > 0 && streetNumber.length() <= 20) {
            return true;
        }
        System.err.println("Invalid street number! The length must be between 1 and 20! ");
        return false;
    }

    public boolean validateTown(String town) {
        if (town != null && !town.trim().isEmpty() && town.length() > 0 && town.length() <= 50) {
            return true;
        }
        System.err.println("Invalid town! Town length must be between 1 and 50! ");
        return false;
    }

    public boolean validatePostalCode(String postalCode) {
        if (postalCode != null && !postalCode.trim().isEmpty() && postalCode.length() > 0 && postalCode.length() <= 20) {
            return true;
        }
        System.err.println("Invalid postal code! Postal code length must be between 1 and 20! ");
        return false;
    }
}
