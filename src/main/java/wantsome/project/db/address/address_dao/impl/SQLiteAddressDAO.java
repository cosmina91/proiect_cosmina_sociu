package wantsome.project.db.address.address_dao.impl;

import wantsome.project.db.address.AddressDTO;
import wantsome.project.db.address.address_dao.AddressDAO;
import wantsome.project.db.service.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLiteAddressDAO implements AddressDAO {
    private static final String INSERT_ADDRESS = "insert into ADDRESS (street, street_number, town, postal_code) values(?, ?, ?, ?);";
    private static final String DELETE_ADDRESS = "delete from ADDRESS where id = ?;";
    private static final String SELECT_ADDRESS_ID = "select id from ADDRESS where street = ? and street_number = ? and town = ? and postal_code = ?;";
    private static final String SELECT_ADDRESS_TO_CHECK_IF_EXISTS = "select * from ADDRESS where street = ? and street_number = ? and town = ? and postal_code = ?;";

    @Override
    public void insert(AddressDTO addressDTO) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_ADDRESS);) {
            int index = 0;
            statement.setString(++index, addressDTO.getStreet());
            statement.setString(++index, addressDTO.getStreetNumber());
            statement.setString(++index, addressDTO.getTown());
            statement.setString(++index, addressDTO.getPostalCode());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting address! " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_ADDRESS);) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting address! " + e.getMessage());
        }
    }

    @Override
    public long selectAddressId(String street, String streetNumber, String town, String postalCode) {
        List<Long> addressIds = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ADDRESS_ID)) {
            statement.setString(1, street);
            statement.setString(2, streetNumber);
            statement.setString(3, town);
            statement.setString(4, postalCode);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                addressIds.add(resultSet.getLong("id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting address id! " + e.getMessage());
        }
        return addressIds.stream().distinct().findFirst().orElse((long) -1);
    }

    @Override
    public AddressDTO selectAddressToCheckIfExists(String street, String streetNumber, String town, String postalCode) {
        AddressDTO address = null;
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ADDRESS_TO_CHECK_IF_EXISTS)) {
            statement.setString(1, street);
            statement.setString(2, streetNumber);
            statement.setString(3, town);
            statement.setString(4, postalCode);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                address = new AddressDTO(resultSet.getLong("id"), resultSet.getString("street"),
                        resultSet.getString("street_number"), resultSet.getString("town"),
                        resultSet.getString("postal_code"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting address! " + e.getMessage());
        }
        return address;
    }
}
