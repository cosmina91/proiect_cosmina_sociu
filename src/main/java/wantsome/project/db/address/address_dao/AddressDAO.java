package wantsome.project.db.address.address_dao;

import wantsome.project.db.address.AddressDTO;

public interface AddressDAO {
    void insert(AddressDTO addressDTO);

    void delete(long id);

    long selectAddressId(String street, String streetNumber, String town, String postalCode);

    AddressDTO selectAddressToCheckIfExists(String street, String streetNumber, String town, String postalCode);
}
