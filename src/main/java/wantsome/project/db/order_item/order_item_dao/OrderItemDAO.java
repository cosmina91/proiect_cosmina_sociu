package wantsome.project.db.order_item.order_item_dao;

import wantsome.project.db.cart_item.CartItemDTO;
import wantsome.project.db.order_item.OrderItemDTO;

import java.util.List;

public interface OrderItemDAO {
    List<OrderItemDTO> selectAllOrderItems();

    List<OrderItemDTO> selectByProductId(long productId);

    void insert(OrderItemDTO orderItemDTO);

    void delete(long id);

    void updateQuantity(long id, long quantity);

    long orderTotalItems();

    List<CartItemDTO> selectCartItems();
}
