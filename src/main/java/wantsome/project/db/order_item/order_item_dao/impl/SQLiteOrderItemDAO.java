package wantsome.project.db.order_item.order_item_dao.impl;

import wantsome.project.db.cart_item.CartItemDTO;
import wantsome.project.db.order_item.OrderItemDTO;
import wantsome.project.db.order_item.order_item_dao.OrderItemDAO;
import wantsome.project.db.service.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLiteOrderItemDAO implements OrderItemDAO {
    private static final String SELECT_ALL_ORDER_ITEMS = "select * from ORDER_ITEM;";
    private static final String SELECT_ORDER_ITEMS_BY_PRODUCT_ID = "select * from ORDER_ITEM where product_id = ?;";
    private static final String SELECT_CART_ITEMS_JOIN = "select o.product_id, p.description, o.quantity, p.price, p.product_path from PRODUCTS p join ORDER_ITEM o on p.id = o.product_id;";
    private static final String INSERT_ORDER_ITEM = "insert into ORDER_ITEM (order_id, product_id, quantity)" +
            "values(?, ?, ?);";
    private static final String DELETE_ORDER_ITEM = "delete from ORDER_ITEM where id = ?;";
    private static final String UPDATE_QUANTITY = "update ORDER_ITEM set quantity = ? where id = ?;";


    @Override
    public List<OrderItemDTO> selectAllOrderItems() {
        List<OrderItemDTO> orderItemDTOList = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_ORDER_ITEMS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderItemDTOList.add(new OrderItemDTO(resultSet.getLong("id"), resultSet.getLong("order_id"), resultSet.getLong("product_id"),
                        resultSet.getLong("quantity")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting all order items! " + e.getMessage());
        }
        return orderItemDTOList;
    }

    @Override
    public List<OrderItemDTO> selectByProductId(long productId) {
        List<OrderItemDTO> orderItemDTOs = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ORDER_ITEMS_BY_PRODUCT_ID)) {
            statement.setLong(1, productId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderItemDTOs.add(new OrderItemDTO(resultSet.getLong("id"), resultSet.getLong("order_id"), resultSet.getLong("product_id"),
                        resultSet.getLong("quantity")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting order item by product id! " + e.getMessage());
        }
        return orderItemDTOs;
    }

    @Override
    public void insert(OrderItemDTO orderItemDTO) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_ORDER_ITEM)) {
            int index = 0;
            statement.setLong(++index, orderItemDTO.getOrderId());
            statement.setLong(++index, orderItemDTO.getProductId());
            statement.setLong(++index, orderItemDTO.getQuantity());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting order item! " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_ORDER_ITEM)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting order item! " + e.getMessage());
        }
    }

    @Override
    public void updateQuantity(long id, long quantity) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_QUANTITY)) {
            statement.setLong(1, quantity);
            statement.setLong(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error updating quantity in order item table! " + e.getMessage());
        }
    }

    @Override
    public long orderTotalItems() {
        long totalItems = 0;
        List<OrderItemDTO> orderItemDTOList = selectAllOrderItems();
        totalItems = orderItemDTOList.stream().mapToLong(OrderItemDTO::getQuantity).sum();
        return totalItems;
    }

    @Override
    public List<CartItemDTO> selectCartItems() {
        List<CartItemDTO> cartItemDTOList = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_CART_ITEMS_JOIN)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                cartItemDTOList.add(new CartItemDTO(resultSet.getLong("product_id"), resultSet.getString("description"), resultSet.getLong("quantity"),
                        resultSet.getDouble("price"), resultSet.getString("product_path")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting cart items (join products and order item tables! " + e.getMessage());
        }
        return cartItemDTOList;
    }
}
