package wantsome.project.db.order_item;

import wantsome.project.db.cart_item.CartItemDTO;
import wantsome.project.db.order_item.order_item_dao.OrderItemDAO;

import java.util.List;

public class OrderItemBL {
    private final OrderItemDAO orderItemDAO;

    public OrderItemBL(OrderItemDAO orderItemDAO) {
        this.orderItemDAO = orderItemDAO;
    }

    public List<OrderItemDTO> getAllOrderItems() {
        return orderItemDAO.selectAllOrderItems();
    }

    public List<OrderItemDTO> getOrderItemByProductId(long productId) {
        return orderItemDAO.selectByProductId(productId);
    }

    public List<CartItemDTO> getCartItems() {
        return orderItemDAO.selectCartItems();
    }

    public void addOrderItem(long orderId, long productId, long quantity) {
        if (!validateOrderId(orderId) || !validateProductId(productId) || !validateQuantity(quantity)) {
            return;
        }
        OrderItemDTO orderItemDTO = new OrderItemDTO(orderId, productId, quantity);
        orderItemDAO.insert(orderItemDTO);
    }

    public void deleteOrderItem(long id) {
        orderItemDAO.delete(id);
    }

    public void updateQuantity(long id, long quantity) {
        if (!validateQuantity(quantity)) {
            return;
        }
        orderItemDAO.updateQuantity(id, quantity);
    }

    public long getTotalQuantity() {
        return orderItemDAO.orderTotalItems();
    }

    private boolean validateOrderId(long orderId) {
        if (orderId > 0) {
            return true;
        }
        System.err.println("Invalid order id! Must be a positive number!");
        return false;
    }

    private boolean validateProductId(long productId) {
        if (productId > 0) {
            return true;
        }
        System.err.println("Invalid product id! Must be a positive number!");
        return false;
    }

    private boolean validateQuantity(long quantity) {
        if (quantity > 0) {
            return true;
        }
        System.err.println("Invalid quantity! Must be a positive number!");
        return false;
    }
}
