package wantsome.project.db.products.products_dao.impl;

import wantsome.project.db.other.ProductType;
import wantsome.project.db.products.ProductsDTO;
import wantsome.project.db.products.products_dao.ProductsDAO;
import wantsome.project.db.service.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLiteProductsDAO implements ProductsDAO {
    private static final String INSERT_PRODUCT = "insert into PRODUCTS (product_type, description, price, stock, product_path)\n" +
            "values(?, ?, ?, ?, ?);";
    private static final String SELECT_ALL_PRODUCTS = "select * from PRODUCTS;";
    private static final String SELECT_PRODUCT_BY_ID = "select * from PRODUCTS where id = ?;";
    private static final String SELECT_PRODUCTS_BY_TYPE = "select * from PRODUCTS where product_type like ?;";
    private static final String UPDATE_STOCK = "update PRODUCTS set stock = ? where id = ?;";
    private static final String SEARCH_PRODUCTS = "select * from PRODUCTS where description like ? or description like ? or description like ?;";


    @Override
    public void insert(ProductsDTO productsDTO) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_PRODUCT);) {
            int index = 0;
            statement.setString(++index, String.valueOf(productsDTO.getProductType()));
            statement.setString(++index, productsDTO.getDescription());
            statement.setDouble(++index, productsDTO.getPrice());
            statement.setInt(++index, productsDTO.getStock());
            statement.setString(++index, productsDTO.getProductPath());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting product! " + e.getMessage());
        }
    }

    @Override
    public List<ProductsDTO> selectAll() {
        List<ProductsDTO> allProducts = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_PRODUCTS);
             ResultSet resultSet = statement.executeQuery();) {
            addingProductsFromResultSetIntoList(resultSet, allProducts);
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting all products! " + e.getMessage());
        }
        return allProducts;
    }

    @Override
    public ProductsDTO selectById(long id) {
        ProductsDTO productsDTO = null;
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_PRODUCT_BY_ID);) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                productsDTO = new ProductsDTO(resultSet.getLong("id"), ProductType.valueOf(resultSet.getString("product_type")), resultSet.getString("description"),
                        resultSet.getDouble("price"), resultSet.getInt("stock"), resultSet.getString("product_path"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting product by id! " + e.getMessage());
        }
        return productsDTO;
    }

    @Override
    public List<ProductsDTO> selectAllByType(ProductType productType) {
        List<ProductsDTO> productsByType = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_PRODUCTS_BY_TYPE);) {
            statement.setString(1, String.valueOf(productType));
            ResultSet resultSet = statement.executeQuery();
            addingProductsFromResultSetIntoList(resultSet, productsByType);
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting products by type! " + e.getMessage());
        }
        return productsByType;
    }

    @Override
    public void updateStock(long id, int newStock) {
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_STOCK);) {
            statement.setInt(1, newStock);
            statement.setLong(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error updating stock in product table! " + e.getMessage());
        }
    }

    @Override
    public List<ProductsDTO> searchProducts(String wordInDescription) {
        String string1 = "%" + wordInDescription;
        String string2 = "%" + wordInDescription + "%";
        String string3 = wordInDescription + "%";

        List<ProductsDTO> searchedProductsList = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SEARCH_PRODUCTS)) {
            statement.setString(1, string1);
            statement.setString(2, string2);
            statement.setString(3, string3);
            ResultSet resultSet = statement.executeQuery();
            addingProductsFromResultSetIntoList(resultSet, searchedProductsList);
        } catch (SQLException e) {
            throw new RuntimeException("Error searching for products! " + e.getMessage());
        }
        return searchedProductsList;
    }

    private void addingProductsFromResultSetIntoList(ResultSet resultSet, List<ProductsDTO> allProducts) throws SQLException {
        while (resultSet.next()) {
            allProducts.add(new ProductsDTO(resultSet.getLong("id"), ProductType.valueOf(resultSet.getString("product_type")), resultSet.getString("description"),
                    resultSet.getDouble("price"), resultSet.getInt("stock"), resultSet.getString("product_path")));
        }
    }
}
