package wantsome.project.db.products.products_dao;

import wantsome.project.db.other.ProductType;
import wantsome.project.db.products.ProductsDTO;

import java.util.List;

public interface ProductsDAO {
    void insert(ProductsDTO productsDTO);

    List<ProductsDTO> selectAll();

    ProductsDTO selectById(long id);

    List<ProductsDTO> selectAllByType(ProductType productType);

    void updateStock(long id, int newStock);

    List<ProductsDTO> searchProducts(String wordInDescription);

}
