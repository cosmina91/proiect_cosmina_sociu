package wantsome.project.db.products;

import wantsome.project.db.other.ProductType;
import wantsome.project.db.products.products_dao.ProductsDAO;

import java.util.List;

public class ProductsBL {
    private final ProductsDAO productsDAO;

    public ProductsBL(ProductsDAO productsDAO) {
        this.productsDAO = productsDAO;
    }

    public void addProduct(ProductType productType, String description, double price, int stock, String productPath) {
        if (!validateProductType(productType) || !validateDescription(description) || !validatePrice(price) || !validateStock(stock)) {
            return;
        }
        ProductsDTO productsDTO = new ProductsDTO(productType, description, price, stock, productPath);
        productsDAO.insert(productsDTO);
    }

    public List<ProductsDTO> searchProducts(String wordInDescription){
        return productsDAO.searchProducts(wordInDescription);
    }

    public List<ProductsDTO> getAllProducts() {
        return productsDAO.selectAll();
    }

    public ProductsDTO getProductById(long id) {
        return productsDAO.selectById(id);
    }

    public List<ProductsDTO> getAllProductsByType(ProductType productType) {
        if (!validateProductType(productType)) {
            return null;
        }
        return productsDAO.selectAllByType(productType);
    }

    public void updateStock(long id, int newStock) {
        if (!validateStock(newStock)) {
            return;
        }
        productsDAO.updateStock(id, newStock);
    }

    private boolean validateProductType(ProductType productType) {
        if (productType != null) {
            return true;
        }
        System.err.println("Invalid product type! ");
        return false;
    }

    private boolean validateDescription(String description) {
        if (description != null && !description.trim().isEmpty() && description.length() > 0 && description.length() <= 100) {
            return true;
        }
        System.err.println("Invalid product description! The length must be between 1 and 100! ");
        return false;
    }

    private boolean validatePrice(double price) {
        if (price >= 0) {
            return true;
        }
        System.err.println("Invalid price! Must be a positive number! ");
        return false;
    }

    private boolean validateStock(int stock) {
        if (stock >= 0) {
            return true;
        }
        System.err.println("Invalid stock! Must be a positive number! ");
        return false;
    }
}
