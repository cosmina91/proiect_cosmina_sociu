package wantsome.project.db.products;

import wantsome.project.db.other.ProductType;

import java.util.Objects;

public class ProductsDTO {
    private long id;
    private ProductType productType;
    private String description;
    private double price;
    private int stock;
    private String productPath;

    public ProductsDTO(long id, ProductType productType, String description, double price, int stock, String productPath) {
        this.id = id;
        this.productType = productType;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.productPath = productPath;
    }

    public ProductsDTO(ProductType productType, String description, double price, int stock, String productPath) {
        this.productType = productType;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.productPath = productPath;
    }

    public long getId() {
        return id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }

    public String getProductPath() {
        return productPath;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductsDTO that = (ProductsDTO) o;
        return id == that.id && Double.compare(that.price, price) == 0 && stock == that.stock && productType == that.productType && Objects.equals(description, that.description) && Objects.equals(productPath, that.productPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productType, description, price, stock, productPath);
    }

    @Override
    public String toString() {
        return "ProductsDTO{" +
                "id=" + id +
                ", productType=" + productType +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", productPath='" + productPath + '\'' +
                '}';
    }
}
