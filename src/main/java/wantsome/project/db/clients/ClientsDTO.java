package wantsome.project.db.clients;

import wantsome.project.db.address.AddressDTO;

import java.util.Objects;

public class ClientsDTO {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private AddressDTO addressDTO;

    public ClientsDTO(long id, String firstName, String lastName, String email, String password, AddressDTO addressDTO) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.addressDTO = addressDTO;
    }

    public ClientsDTO(String firstName, String lastName, String email, String password, AddressDTO addressDTO) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.addressDTO = addressDTO;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public AddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddressDTO(AddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }

    @Override
    public String toString() {
        return "ClientsDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", addressDTO=" + addressDTO +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientsDTO that = (ClientsDTO) o;
        return id == that.id && Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName) && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(addressDTO, that.addressDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, password, addressDTO);
    }
}
