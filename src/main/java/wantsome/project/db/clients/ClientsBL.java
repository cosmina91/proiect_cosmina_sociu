package wantsome.project.db.clients;

import wantsome.project.db.address.AddressDTO;
import wantsome.project.db.clients.clients_dao.ClientsDAO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientsBL {
    private final ClientsDAO clientsDAO;

    public ClientsBL(ClientsDAO clientsDAO) {
        this.clientsDAO = clientsDAO;
    }

    public void addClient(String firstName, String lastName, String email, String password, AddressDTO addressDTO) {
        if (!validateFirstName(firstName) || !validateLastName(lastName)
                || !validateEmail(email) || !validatePassword(password)) {
            System.err.println("Error adding client in DB!");
            return;
        }

        ClientsDTO clientsDTO = new ClientsDTO(firstName, lastName, email, password, addressDTO);
        clientsDAO.insert(clientsDTO);
    }

    public long getClientId(String email) {
        return clientsDAO.selectClientId(email);
    }

    public ClientsDTO getClientByEmail(String email) {
        return clientsDAO.selectClientByEmail(email);
    }

    public boolean validateFirstName(String firstName) {
        if (firstName != null && !firstName.trim().isEmpty() && firstName.length() > 0 && firstName.length() <= 50) {
            return true;
        }
        System.err.println("Invalid first name! The length must be between 1 and 50! ");
        return false;
    }

    public boolean validateLastName(String lastName) {
        if (lastName != null && !lastName.trim().isEmpty() && lastName.length() > 0 && lastName.length() <= 50) {
            return true;
        }
        System.err.println("Invalid last name! The length must be between 1 and 50! ");
        return false;
    }

    public boolean validateEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);

        if (m.matches() && email.length() <= 100 && email.length() >= 5) {
            return true;
        }
        System.err.println("Invalid email! The length must be between 5 and 100 and must contain \"@\" character! ");
        return false;
    }

    public boolean validatePassword(String password) {
        String regex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!.?]).{6,15}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        if (matcher.matches()) {
            return true;
        }
        System.err.println("Invalid password! The password must be between 6 to 15 characters which contain at least one lowercase letter," +
                " one uppercase letter, one numeric digit, and one special character! ");
        return false;
    }

}
