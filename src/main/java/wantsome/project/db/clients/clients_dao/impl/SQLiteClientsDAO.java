package wantsome.project.db.clients.clients_dao.impl;

import wantsome.project.db.address.AddressDTO;
import wantsome.project.db.address.address_dao.AddressDAO;
import wantsome.project.db.address.address_dao.impl.SQLiteAddressDAO;
import wantsome.project.db.clients.ClientsDTO;
import wantsome.project.db.clients.clients_dao.ClientsDAO;
import wantsome.project.db.service.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLiteClientsDAO implements ClientsDAO {
    private static final String INSERT_CLIENT = "insert into CLIENTS (first_name, last_name, email, password, address_id)" +
            "values(?, ?, ?, ?, ?);";
    private static final String SELECT_CLIENT_ID = "select id from CLIENTS where email = ?;";
    private static final String SELECT_CLIENT_BY_EMAIL = "select c.id, c.first_name, c.last_name, c.email, c.password, a.street, \n" +
            "a.street_number, a.town, a.postal_code from CLIENTS c join ADDRESS a on c.address_id = a.id where email = ?;";

    @Override
    public void insert(ClientsDTO clientsDTO) {
        AddressDAO addressDAO = new SQLiteAddressDAO();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_CLIENT)) {
            int index = 0;
            statement.setString(++index, clientsDTO.getFirstName());
            statement.setString(++index, clientsDTO.getLastName());
            statement.setString(++index, clientsDTO.getEmail());
            statement.setString(++index, clientsDTO.getPassword());
            AddressDTO addressDTO = new AddressDTO(clientsDTO.getAddressDTO().getStreet(), clientsDTO.getAddressDTO().getStreetNumber(),
                    clientsDTO.getAddressDTO().getTown(), clientsDTO.getAddressDTO().getPostalCode());
            AddressDTO address = addressDAO.selectAddressToCheckIfExists(addressDTO.getStreet(), addressDTO.getStreetNumber(),
                    addressDTO.getTown(), addressDTO.getPostalCode());
            if (address == null) {
                addressDAO.insert(addressDTO);
            }
            long addressId = addressDAO.selectAddressId(addressDTO.getStreet(), addressDTO.getStreetNumber(), addressDTO.getTown(), addressDTO.getPostalCode());
            statement.setLong(++index, addressId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting client! " + e.getMessage());
        }
    }

    @Override
    public long selectClientId(String email) {
        List<Long> clientIds = new ArrayList<>();
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_CLIENT_ID)) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                clientIds.add(resultSet.getLong("id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting client id! " + e.getMessage());
        }
        return clientIds.stream().distinct().findFirst().orElse((long) -1);
    }

    @Override
    public ClientsDTO selectClientByEmail(String email) {
        ClientsDTO clientDTO = null;
        try (Connection connection = DbManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_CLIENT_BY_EMAIL)) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                AddressDTO addressDTO = new AddressDTO(resultSet.getString("street"), resultSet.getString("street_number"),
                        resultSet.getString("town"), resultSet.getString("postal_code"));
                clientDTO = new ClientsDTO(resultSet.getLong("id"), resultSet.getString("first_name"),
                        resultSet.getString("last_name"), resultSet.getString("email"), resultSet.getString("password"),
                        addressDTO);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error selecting client id! " + e.getMessage());
        }
        return clientDTO;
    }
}
