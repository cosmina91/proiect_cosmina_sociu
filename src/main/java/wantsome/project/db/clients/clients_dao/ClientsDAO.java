package wantsome.project.db.clients.clients_dao;

import wantsome.project.db.clients.ClientsDTO;

public interface ClientsDAO {
    void insert(ClientsDTO clientsDTO);

    long selectClientId(String email);

    ClientsDTO selectClientByEmail(String email);
}
