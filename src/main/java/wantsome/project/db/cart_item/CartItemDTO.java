package wantsome.project.db.cart_item;

import java.util.Objects;

public class CartItemDTO {
    private long productId;
    private String description;
    private long quantity;
    private double price;
    private String productPath;

    public CartItemDTO(long productId, String description, long quantity, double price, String productPath) {
        this.productId = productId;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.productPath = productPath;
    }


    public long getProductId() {
        return productId;
    }

    public String getDescription() {
        return description;
    }

    public long getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getProductPath() {
        return productPath;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CartItemDTO{" +
                "productId=" + productId +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", productPath='" + productPath + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItemDTO that = (CartItemDTO) o;
        return productId == that.productId && quantity == that.quantity && Double.compare(that.price, price) == 0 && Objects.equals(description, that.description) && Objects.equals(productPath, that.productPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, description, quantity, price, productPath);
    }
}
