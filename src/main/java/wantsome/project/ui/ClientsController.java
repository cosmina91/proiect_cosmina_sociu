package wantsome.project.ui;

import io.javalin.http.Context;
import wantsome.project.db.address.AddressBL;
import wantsome.project.db.address.AddressDTO;
import wantsome.project.db.cart_item.CartItemDTO;
import wantsome.project.db.clients.ClientsBL;
import wantsome.project.db.clients.ClientsDTO;
import wantsome.project.db.order_item.OrderItemBL;
import wantsome.project.db.order_item.order_item_dao.OrderItemDAO;
import wantsome.project.db.order_item.order_item_dao.impl.SQLiteOrderItemDAO;
import wantsome.project.db.orders.OrdersBL;
import wantsome.project.db.orders.OrdersDTO;
import wantsome.project.db.orders.orders_dao.OrdersDAO;
import wantsome.project.db.orders.orders_dao.impl.SQLiteOrdersDAO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ClientsController {
    private final ClientsBL clientsBL;
    private final AddressBL addressBL;
    private static boolean emailIsValid = true;
    private static boolean firstNameIsValid = true;
    private static boolean lastNameIsValid = true;
    private static boolean streetIsValid = true;
    private static boolean streetNumberIsValid = true;
    private static boolean townIsValid = true;
    private static boolean postalCodeIsValid = true;

    private static boolean cardNumberIsValid = true;
    private static boolean nameOnCardIsValid = true;
    private static boolean expirationDate = true;
    private static boolean cvvIsValid = true;
    private static ClientsDTO clientDTO = new ClientsDTO("", "", "","", new AddressDTO("", "", "", ""));
    private static long orderId = 0L;
    private static String cardNumber = "";
    private static String nameOnCard = "";
    private static String cardCVV = "";

    public ClientsController(ClientsBL clientsBL, AddressBL addressBL) {
        this.clientsBL = clientsBL;
        this.addressBL = addressBL;
    }

    public ClientsBL getClientsBL() {
        return clientsBL;
    }

    public static void setEmailIsValid(boolean emailIsValid) {
        ClientsController.emailIsValid = emailIsValid;
    }

    public static void setFirstNameIsValid(boolean firstNameIsValid) {
        ClientsController.firstNameIsValid = firstNameIsValid;
    }

    public static void setLastNameIsValid(boolean lastNameIsValid) {
        ClientsController.lastNameIsValid = lastNameIsValid;
    }

    public static void setStreetIsValid(boolean streetIsValid) {
        ClientsController.streetIsValid = streetIsValid;
    }

    public static void setStreetNumberIsValid(boolean streetNumberIsValid) {
        ClientsController.streetNumberIsValid = streetNumberIsValid;
    }

    public static void setTownIsValid(boolean townIsValid) {
        ClientsController.townIsValid = townIsValid;
    }

    public static void setPostalCodeIsValid(boolean postalCodeIsValid) {
        ClientsController.postalCodeIsValid = postalCodeIsValid;
    }

    public static void setCardNumberIsValid(boolean cardNumberIsValid) {
        ClientsController.cardNumberIsValid = cardNumberIsValid;
    }

    public static void setNameOnCardIsValid(boolean nameOnCardIsValid) {
        ClientsController.nameOnCardIsValid = nameOnCardIsValid;
    }

    public static void setExpirationDate(boolean expirationDate) {
        ClientsController.expirationDate = expirationDate;
    }

    public static void setCvvIsValid(boolean cvvIsValid) {
        ClientsController.cvvIsValid = cvvIsValid;
    }

    public void getOrderForm(Context ctx) {
        Map<String, Object> model = new HashMap<>();
        model.put("mailIsValid", emailIsValid);
        model.put("firstNameValid", firstNameIsValid);
        model.put("lastNameValid", lastNameIsValid);
        model.put("streetIsValid", streetIsValid);
        model.put("streetNrIsValid", streetNumberIsValid);
        model.put("townIsValid", townIsValid);
        model.put("postalCodeIsValid", postalCodeIsValid);
        model.put("cartItemList", ProductsController.cartItemDTOList);
        model.put("totalPrice", ProductsController.totalPrice);
        model.put("clientDTO" , clientDTO);
        ctx.render("order_form.vm", model);
    }

    public void validateClientInfo(Context ctx) {
        String email = ctx.formParam("email");
        String firstName = ctx.formParam("firstName");
        String lastName = ctx.formParam("lastName");
        String street = ctx.formParam("street");
        String streetNumber = ctx.formParam("streetNumber");
        String town = ctx.formParam("town");
        String postalCode = ctx.formParam("postalCode");
        clientDTO = new ClientsDTO(firstName, lastName, email, "An123!", new AddressDTO(street, streetNumber, town, postalCode));

        boolean mailIsValid = clientsBL.validateEmail(email);
        boolean firstNameValid = clientsBL.validateFirstName(firstName);
        boolean lastNameValid = clientsBL.validateLastName(lastName);
        boolean streetIsValid = addressBL.validateStreet(street);
        boolean streetNrIsValid = addressBL.validateStreetNumber(streetNumber);
        boolean townIsValid = addressBL.validateTown(town);
        boolean postalCodeIsValid = addressBL.validatePostalCode(postalCode);


        int count = 0;
        if (!mailIsValid) {
            setEmailIsValid(false);
            count++;
        } else {
            setEmailIsValid(true);
        }

        if (!firstNameValid) {
            setFirstNameIsValid(false);
            count++;
        } else {
            setFirstNameIsValid(true);
        }

        if (!lastNameValid) {
            setLastNameIsValid(false);
            count++;
        } else {
            setLastNameIsValid(true);
        }

        if (!streetIsValid) {
            setStreetIsValid(false);
            count++;
        } else {
            setStreetIsValid(true);
        }

        if (!streetNrIsValid) {
            setStreetNumberIsValid(false);
            count++;
        } else {
            setStreetNumberIsValid(true);
        }

        if (!townIsValid) {
            setTownIsValid(false);
            count++;
        } else {
            setTownIsValid(true);
        }

        if (!postalCodeIsValid) {
            setPostalCodeIsValid(false);
            count++;
        } else {
            setPostalCodeIsValid(true);
        }

        if (count > 0) {
            ctx.redirect("/orderForm");
        } else {
            ctx.redirect("/pay");
        }
    }

    public void verifyCardInfo(Context ctx) throws ParseException {
        cardNumber = ctx.formParam("card_number");
        nameOnCard = ctx.formParam("card_name");
        String expireMonth = ctx.formParam("expireMM");
        String expireYear = ctx.formParam("expireYY");
        cardCVV = ctx.formParam("card_CVV");

        int count = 0;
        if (!validateCardNumber(cardNumber)) {
            setCardNumberIsValid(false);
            count++;
        } else {
            setCardNumberIsValid(true);
        }

        if (!validateNameOnCard(nameOnCard)) {
            setNameOnCardIsValid(false);
            count++;
        } else {
            setNameOnCardIsValid(true);
        }

        if (!validateExpirationDate(expireMonth, expireYear)) {
            setExpirationDate(false);
            count++;
        } else {
            setExpirationDate(true);
        }

        if (!validateCVV(cardCVV)) {
            setCvvIsValid(false);
            count++;
        } else {
            setCvvIsValid(true);
        }

        if (count > 0) {
            ctx.redirect("/pay");
        } else {
            ClientsDTO client = clientsBL.getClientByEmail(clientDTO.getEmail());
            if (client == null) {
                clientsBL.addClient(clientDTO.getFirstName(), clientDTO.getLastName(), clientDTO.getEmail(), clientDTO.getPassword(),
                        clientDTO.getAddressDTO());
            }
            OrdersDAO ordersDAO = new SQLiteOrdersDAO();
            OrdersBL ordersBL = new OrdersBL(ordersDAO);
            long clientId = clientsBL.getClientId(clientDTO.getEmail());
            java.sql.Date currentDate = new java.sql.Date(System.currentTimeMillis());
            ordersBL.addOrder(clientId, currentDate, ProductsController.totalPrice);
            orderId = ordersBL.getOrderId(clientId, currentDate, ProductsController.totalPrice);

            OrderItemDAO orderItemDAO = new SQLiteOrderItemDAO();
            OrderItemBL orderItemBL = new OrderItemBL(orderItemDAO);

            for (CartItemDTO item : ProductsController.cartItemDTOList) {
                orderItemBL.addOrderItem(orderId, item.getProductId(), item.getQuantity());
            }
            cardNumber = "";
            nameOnCard = "";
            cardCVV = "";
            ctx.redirect("/invoice");
        }
    }

    private boolean validateNameOnCard(String name) {
        if (name.length() > 2) {
            return true;
        }
        return false;
    }

    private boolean validateExpirationDate(String month, String year) {
        Long currentTime = System.currentTimeMillis();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
        Date date = new Date(currentTime);
        String currentDate = simpleDateFormat.format(date);
        String cardDate = year + month;
        System.out.println("Current date: " + currentDate + ", card date: " + cardDate);
        boolean isValid;
        try {
            isValid = Long.parseLong(currentDate) <= Long.parseLong(cardDate);
        } catch (NumberFormatException ex) {
            System.out.println("Error parsing date! " + ex.getMessage());
            isValid = false;
        }
        return isValid;
    }

    private boolean validateCVV(String CVV) {
        try {
            int cvv = Integer.parseInt(CVV);
        } catch (NumberFormatException ex) {
            System.out.println("Invalid CVV! " + ex.getMessage());
            return false;
        }
        if (CVV.length() == 3) {
            return true;
        }
        return false;
    }

    private boolean validateCardNumber(String cardNumber) {
        try {
            Long.parseLong(cardNumber);
        } catch (NumberFormatException ex) {
            System.out.println("Invalid card number! " + ex.getMessage());
            return false;
        }
        if (cardNumber.length() == 16) {
            return true;
        }
        return false;
    }

    public void getPaymentPage(Context ctx) {
        Map<String, Object> model = new HashMap<>();
        model.put("cardNrIsValid", cardNumberIsValid);
        model.put("nameIsValid", nameOnCardIsValid);
        model.put("expireDateIsValid", expirationDate);
        model.put("cvvIsValid", cvvIsValid);
        model.put("cardNumber", cardNumber);
        model.put("nameOnCard", nameOnCard);
        model.put("cardCVV", cardCVV);

        Long currentTime = System.currentTimeMillis();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        Date date = new Date(currentTime);
        String currentYear = simpleDateFormat.format(date);
        String year1 = String.valueOf(Integer.parseInt(currentYear) + 1);
        String year2 = String.valueOf(Integer.parseInt(currentYear) + 2);
        String year3 = String.valueOf(Integer.parseInt(currentYear) + 3);
        String year4 = String.valueOf(Integer.parseInt(currentYear) + 4);
        model.put("currentYear", currentYear);
        model.put("year1", year1);
        model.put("year2", year2);
        model.put("year3", year3);
        model.put("year4", year4);

        ctx.render("pay.vm", model);
    }

    public void getInvoicePage(Context ctx) {

        OrdersDAO ordersDAO = new SQLiteOrdersDAO();
        OrdersBL ordersBL = new OrdersBL(ordersDAO);
        OrdersDTO ordersDTO = ordersBL.getOrderDetailsById(orderId);

        long totalQuantity = ProductsController.cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();

        Map<String, Object> model = new HashMap<>();
        model.put("client", clientDTO);
        model.put("clientAddress", clientDTO.getAddressDTO());
        model.put("orderDTO", ordersDTO);
        model.put("productsOrder", ProductsController.cartItemDTOList);
        model.put("totalQuantity", totalQuantity);

        ctx.render("invoice.vm", model);
    }

    public void backToMainPage(Context ctx) {
        ProductsController.cartItemDTOList = new ArrayList<>();
        clientDTO = new ClientsDTO("", "", "", "", new AddressDTO("", "", "", ""));
        ctx.redirect("/main");
    }
}
