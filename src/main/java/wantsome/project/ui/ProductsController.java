package wantsome.project.ui;

import io.javalin.http.Context;
import wantsome.project.db.cart_item.CartItemDTO;
import wantsome.project.db.order_item.OrderItemBL;
import wantsome.project.db.other.ProductType;
import wantsome.project.db.products.ProductsBL;
import wantsome.project.db.products.ProductsDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsController {
    private final ProductsBL productsBL;
    private final OrderItemBL orderItemBL;
    protected static List<CartItemDTO> cartItemDTOList = new ArrayList<>();
    protected static double totalPrice = 0;


    public ProductsController(ProductsBL productsBL, OrderItemBL orderItemBL) {
        this.productsBL = productsBL;
        this.orderItemBL = orderItemBL;
    }

    public void getAllProducts(Context ctx) {
        List<ProductsDTO> allProducts = productsBL.getAllProducts();
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("products", allProducts);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getAllRings(Context ctx) {
        List<ProductsDTO> ringsList = productsBL.getAllProductsByType(ProductType.INELE);
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("products", ringsList);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getAllBracelets(Context ctx) {
        List<ProductsDTO> braceletsList = productsBL.getAllProductsByType(ProductType.BRATARI);
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("products", braceletsList);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getAllEarrings(Context ctx) {
        List<ProductsDTO> earringsList = productsBL.getAllProductsByType(ProductType.CERCEI);
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("products", earringsList);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getAllNecklaces(Context ctx) {
        List<ProductsDTO> necklacesList = productsBL.getAllProductsByType(ProductType.COLIERE);
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("products", necklacesList);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getAllPendants(Context ctx) {
        List<ProductsDTO> pendantsList = productsBL.getAllProductsByType(ProductType.PANDANTIVE);
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("products", pendantsList);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getAllCartItems(Context ctx) {
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        cartItemDTOList.stream().forEach(p -> {
            ProductsDTO productDTO = productsBL.getProductById(p.getProductId());
            double priceFor1item = productDTO.getPrice();
            p.setPrice(priceFor1item * p.getQuantity());   //using the price setter to update the price when the quantity is > 1;
        });
        totalPrice = cartItemDTOList.stream().mapToDouble(CartItemDTO::getPrice).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("cartItemList", cartItemDTOList);
        model.put("totalPrice", totalPrice);
        model.put("totalQuantity", totalQuantity);
        ctx.render("/cart.vm", model);
    }

    public void getSearchedProducts(ProductsBL productsBL, Context ctx) {
        Map<String, Object> model = new HashMap<>();
        String wordSearch = ctx.formParam("search2");
        List<ProductsDTO> searchedProducts = productsBL.searchProducts(wordSearch);
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        model.put("products", searchedProducts);
        model.put("totalQuantity", totalQuantity);
        ctx.render("products.vm", model);
    }

    public void getTotalQuantity(Context ctx) {
        long totalQuantity = cartItemDTOList.stream().mapToLong(CartItemDTO::getQuantity).sum();
        Map<String, Object> model = new HashMap<>();
        model.put("totalQuantity", totalQuantity);
        ctx.render("main.vm", model);
    }

    public void addItemToCartList(Context ctx) {
        String productId = ctx.formParam("productId");
        long prodId = Long.parseLong(productId);
        String description = ctx.formParam("description");
        String quantity = ctx.formParam("quantity");
        long quantityItem = Long.parseLong(quantity);
        String price = ctx.formParam("price");
        double priceItem = Double.parseDouble(price);
        String productPath = ctx.formParam("productPath");
        CartItemDTO cartItemDTO = new CartItemDTO(prodId, description, quantityItem, priceItem, productPath);
        for (CartItemDTO item : cartItemDTOList) {
            if (item.getProductId() == prodId) {
                item.setQuantity(item.getQuantity() + quantityItem);
                ProductsDTO productDTO = productsBL.getProductById(item.getProductId());
                productsBL.updateStock(item.getProductId(), productDTO.getStock() - (int)quantityItem);
                ctx.redirect("/cart");
                return;
            }
        }
        cartItemDTOList.add(cartItemDTO);
        ProductsDTO productDTO = productsBL.getProductById(cartItemDTO.getProductId());
        productsBL.updateStock(cartItemDTO.getProductId(), productDTO.getStock() - (int)quantityItem);
        ctx.redirect("/cart");
    }

    public void removeItemFromCartList(Context ctx) {
        String idProduct = ctx.formParam("idProduct");
        long id = Long.parseLong(idProduct);
        CartItemDTO cartItemDTO = null;

        for (CartItemDTO item : cartItemDTOList) {
            if (item.getProductId() == id) {
                cartItemDTO = item;
            }
        }
        if (cartItemDTO != null) {
            cartItemDTOList.remove(cartItemDTO);
            ProductsDTO productDTO = productsBL.getProductById(cartItemDTO.getProductId());
            productsBL.updateStock(cartItemDTO.getProductId(), productDTO.getStock() + (int)cartItemDTO.getQuantity());
        }
        ctx.redirect("/cart");
    }

}
